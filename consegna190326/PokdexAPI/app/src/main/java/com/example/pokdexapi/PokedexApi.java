package com.example.pokdexapi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PokedexApi {

    @GET("GET_ALL")
    Call<List<Pokemon>> getPokemon();

    @GET("GET_BY_NUM")
    Call<List<Pokemon>> getByNum(@Query("num") String num);

}
