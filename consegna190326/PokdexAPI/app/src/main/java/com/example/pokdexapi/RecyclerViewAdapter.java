package com.example.pokdexapi;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> nums = new ArrayList<>();
    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<String> types1 = new ArrayList<>();
    private ArrayList<String> types2 = new ArrayList<>();
    private Context context;

    public RecyclerViewAdapter(Context context, ArrayList<String> names, ArrayList<String> nums, ArrayList<String> images, ArrayList<String> types1, ArrayList<String> types2){
        this.context = context;
        this.names = names;
        this.nums = nums;
        this.images = images;
        this.types1 = types1;
        this.types2 = types2;
    }

    //INFLATING THE VIEWHOLDER
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);

        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    //PASSING INFORMATION TO EACH HOLDER
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        final String index = nums.get(i);

        //Using glide to get the image from the web

        Glide.with(this.context)                //Specifying the context
                .asBitmap()                     //Using bitmaps images
                .load(this.images.get(i))       //Getting the image from the url
                .into(viewHolder.image);        //Targetting where the image needs to go

        //Setting name and num

        viewHolder.name.setText(this.names.get(i));
        viewHolder.num.setText(this.nums.get(i));

        //Setting the types

        switch(this.types1.get(i)) {
            case "Bug":
                viewHolder.type1.setImageResource(R.drawable.bug);
                break;
            case "Dark":
                viewHolder.type1.setImageResource(R.drawable.dark);
                break;
            case "Dragon":
                viewHolder.type1.setImageResource(R.drawable.dragon);
                break;
            case "Electric":
                viewHolder.type1.setImageResource(R.drawable.electric);
                break;
            case "Fairy":
                viewHolder.type1.setImageResource(R.drawable.fairy);
                break;
            case "Fighting":
                viewHolder.type1.setImageResource(R.drawable.fighting);
                break;
            case "Fire":
                viewHolder.type1.setImageResource(R.drawable.fire);
                break;
            case "Flying":
                viewHolder.type1.setImageResource(R.drawable.flying);
                break;
            case "Ghost":
                viewHolder.type1.setImageResource(R.drawable.ghost);
                break;
            case "Grass":
                viewHolder.type1.setImageResource(R.drawable.grass);
                break;
            case "Ground":
                viewHolder.type1.setImageResource(R.drawable.ground);
                break;
            case "Ice":
                viewHolder.type1.setImageResource(R.drawable.ice);
                break;
            case "Normal":
                viewHolder.type1.setImageResource(R.drawable.normal);
                break;
            case "Psychic":
                viewHolder.type1.setImageResource(R.drawable.physic);
                break;
            case "Poison":
                viewHolder.type1.setImageResource(R.drawable.poison);
                break;
            case "Rock":
                viewHolder.type1.setImageResource(R.drawable.rock);
                break;
            case "Stell":
                viewHolder.type1.setImageResource(R.drawable.steel);
                break;
            case "Water":
                viewHolder.type1.setImageResource(R.drawable.water);
                break;
            default:
                viewHolder.type1.setImageResource(R.color.colorAccent);
        }

        switch(this.types2.get(i)) {
            case "Bug":
                viewHolder.type2.setImageResource(R.drawable.bug);
                break;
            case "Dark":
                viewHolder.type2.setImageResource(R.drawable.dark);
                break;
            case "Dragon":
                viewHolder.type2.setImageResource(R.drawable.dragon);
                break;
            case "Electric":
                viewHolder.type2.setImageResource(R.drawable.electric);
                break;
            case "Fairy":
                viewHolder.type2.setImageResource(R.drawable.fairy);
                break;
            case "Fighting":
                viewHolder.type2.setImageResource(R.drawable.fighting);
                break;
            case "Fire":
                viewHolder.type2.setImageResource(R.drawable.fire);
                break;
            case "Flying":
                viewHolder.type2.setImageResource(R.drawable.flying);
                break;
            case "Ghost":
                viewHolder.type2.setImageResource(R.drawable.ghost);
                break;
            case "Grass":
                viewHolder.type2.setImageResource(R.drawable.grass);
                break;
            case "Ground":
                viewHolder.type2.setImageResource(R.drawable.ground);
                break;
            case "Ice":
                viewHolder.type2.setImageResource(R.drawable.ice);
                break;
            case "Normal":
                viewHolder.type2.setImageResource(R.drawable.normal);
                break;
            case "Psychic":
                viewHolder.type2.setImageResource(R.drawable.physic);
                break;
            case "Poison":
                viewHolder.type2.setImageResource(R.drawable.poison);
                break;
            case "Rock":
                viewHolder.type2.setImageResource(R.drawable.rock);
                break;
            case "Stell":
                viewHolder.type2.setImageResource(R.drawable.steel);
                break;
            case "Water":
                viewHolder.type2.setImageResource(R.drawable.water);
                break;
            default:
                viewHolder.type2.setImageResource(R.color.colorAccent);
        }

        //Setting an onClickListenter on the item

        viewHolder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPokemonActivity(index);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.nums.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        ImageView type1;
        ImageView type2;
        TextView name;
        TextView num;
        RelativeLayout itemLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            type1 = itemView.findViewById(R.id.type1);
            type2 = itemView.findViewById(R.id.type2);
            name = itemView.findViewById(R.id.name);
            num = itemView.findViewById(R.id.num);
            itemLayout = itemView.findViewById(R.id.itemLayout);
        }
    }

    public void openPokemonActivity (String num){
        Toast.makeText(this.context, "opening pokemon num:" + num, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this.context, PokemonDescription.class);
        intent.putExtra("num", num);
        this.context.startActivity(intent);
    }

    public void updateList (List<String> newNames, List<String> newNums, List<String> newImages, List<String> newTypes1, List<String> newTypes2){
        this.names = new ArrayList<>();
        this.names.addAll(newNames);
        this.nums = new ArrayList<>();
        this.nums.addAll(newNums);
        this.images = new ArrayList<>();
        this.images.addAll(newImages);
        this.types1 = new ArrayList<>();
        this.types1.addAll(newTypes1);
        this.types2 = new ArrayList<>();
        this.types2.addAll(newTypes2);
        notifyDataSetChanged();
    }
}
