package com.example.pokdexapi;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private RecyclerViewAdapter adapter;
    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> nums = new ArrayList<>();
    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<String> types1 = new ArrayList<>();
    private ArrayList<String> types2 = new ArrayList<>();
    private Context context = this;
    private TextView error_msg;
    private Button reload;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.error_msg = findViewById(R.id.error_msg);
        this.reload = findViewById(R.id.reload);
        this.reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                estabilishConnection();
            }
        });

        estabilishConnection();

    }

    public void estabilishConnection(){

        Toast.makeText(this.context, "opening the pokedex", Toast.LENGTH_SHORT).show();

        error_msg.setVisibility(View.INVISIBLE);
        reload.setVisibility(View.INVISIBLE);
        reload.setClickable(false);

        //RETROFIT 2 GET_ALL CALL

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://webhooks.mongodb-stitch.com/api/client/v2.0/app/pokedex-rrtrm/service/CRUD/incoming_webhook/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PokedexApi pokedexApi = retrofit.create(PokedexApi.class);

        Call<List<Pokemon>> call = pokedexApi.getPokemon();

        call.enqueue(new Callback<List<Pokemon>>() {
            @Override
            public void onResponse(Call<List<Pokemon>> call, Response<List<Pokemon>> response) {

                if(!response.isSuccessful()){

                    error_msg.setVisibility(View.VISIBLE);
                    reload.setVisibility(View.VISIBLE);
                    reload.setClickable(true);

                    return;
                }

                List<Pokemon> pokemons = response.body();

                for (Pokemon pokemon: pokemons){
                    names.add(pokemon.getName());
                    nums.add(pokemon.getNum());
                    images.add(pokemon.getImg());
                    types1.add(pokemon.getType().get(0));
                    try{
                        types2.add(pokemon.getType().get(1));
                    }catch(IndexOutOfBoundsException e){
                        types2.add("null");
                    }
                }

                //CREATING THE RECYCLER VIEW

                RecyclerView recyclerView = findViewById(R.id.recycler_view);
                adapter = new RecyclerViewAdapter(context, names, nums, images, types1, types2);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));


            }

            @Override
            public void onFailure(Call<List<Pokemon>> call, Throwable t) {
                error_msg.setVisibility(View.VISIBLE);
                reload.setVisibility(View.VISIBLE);
                reload.setClickable(true);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.search_icon);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String userInput = newText.toLowerCase();
        List<String> newNames = new ArrayList<>();
        List<String> newNums = new ArrayList<>();
        List<String> newImages = new ArrayList<>();
        List<String> newTypes1 = new ArrayList<>();
        List<String> newTypes2 = new ArrayList<>();

        for(int i=0; i<names.size(); i++){
            if(names.get(i).toLowerCase().contains(userInput)){
                newNames.add(names.get(i));
                newNums.add(nums.get(i));
                newImages.add(images.get(i));
                newTypes1.add(types1.get(i));
                newTypes2.add(types2.get(i));
            }
        }

        adapter.updateList(newNames, newNums, newImages, newTypes1, newTypes2);

        return true;
    }
}
