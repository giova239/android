package com.example.pokdexapi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PokemonDescription extends AppCompatActivity {

    private RelativeLayout holder;
    private TextView pokemon_number;
    private TextView pokemon_name;
    private CircleImageView pokemon_image;
    private TextView pokemon_height;
    private TextView pokemon_weight;
    private List<ImageView> pokemon_types = new ArrayList<>();
    private List<ImageView> pokemon_weaknesses = new ArrayList<>();;
    private List<CircleImageView> pokemon_prev = new ArrayList<>();;
    private List<CircleImageView> pokemon_next = new ArrayList<>();;
    private Context context;

    private TextView error_msg;
    private Button reload;
    private String num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_description);

        this.context = this;
        this.holder = findViewById(R.id.holder);
        this.pokemon_number = findViewById(R.id.pokemon_number);
        this.pokemon_name = findViewById(R.id.pokemon_name);
        this.pokemon_image = findViewById(R.id.pokemon_image);
        this.pokemon_height = findViewById(R.id.pokemon_height);
        this.pokemon_weight = findViewById(R.id.pokemon_weight);
        this.pokemon_types.add((ImageView) findViewById(R.id.pokemon_type1));
        this.pokemon_types.add((ImageView) findViewById(R.id.pokemon_type2));
        this.pokemon_weaknesses.add((ImageView) findViewById(R.id.pokemon_weakness1));
        this.pokemon_weaknesses.add((ImageView) findViewById(R.id.pokemon_weakness2));
        this.pokemon_weaknesses.add((ImageView) findViewById(R.id.pokemon_weakness3));
        this.pokemon_weaknesses.add((ImageView) findViewById(R.id.pokemon_weakness4));
        this.pokemon_weaknesses.add((ImageView) findViewById(R.id.pokemon_weakness5));
        this.pokemon_weaknesses.add((ImageView) findViewById(R.id.pokemon_weakness6));
        this.pokemon_weaknesses.add((ImageView) findViewById(R.id.pokemon_weakness7));
        this.pokemon_next.add((CircleImageView) findViewById(R.id.pokemon_next1));
        this.pokemon_next.add((CircleImageView) findViewById(R.id.pokemon_next2));
        this.pokemon_next.add((CircleImageView) findViewById(R.id.pokemon_next3));
        this.pokemon_prev.add((CircleImageView) findViewById(R.id.pokemon_prev1));
        this.pokemon_prev.add((CircleImageView) findViewById(R.id.pokemon_prev2));

        Intent intent = getIntent();
        this.num = intent.getStringExtra("num");

        this.error_msg = findViewById(R.id.error_msg);
        this.reload = findViewById(R.id.reload);
        this.reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                estabilishConnection();
            }
        });

        estabilishConnection();
    }

    public void estabilishConnection(){

        error_msg.setVisibility(View.INVISIBLE);
        reload.setVisibility(View.INVISIBLE);
        reload.setClickable(false);

        //RETROFIT 2 GET_ALL CALL

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://webhooks.mongodb-stitch.com/api/client/v2.0/app/pokedex-rrtrm/service/CRUD/incoming_webhook/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PokedexApi pokedexApi = retrofit.create(PokedexApi.class);

        Call<List<Pokemon>> call = pokedexApi.getByNum(this.num);

        call.enqueue(new Callback<List<Pokemon>>() {
            @Override
            public void onResponse(Call<List<Pokemon>>call, Response<List<Pokemon>> response) {

                if(!response.isSuccessful()){

                    error_msg.setVisibility(View.VISIBLE);
                    reload.setVisibility(View.VISIBLE);
                    reload.setClickable(true);
                    holder.setEnabled(false);
                    holder.setVisibility(View.INVISIBLE);

                    return;
                }

                holder.setEnabled(true);
                holder.setVisibility(View.VISIBLE);

                //Getting the pokemon
                List<Pokemon> pokemon = response.body();
                final Pokemon p = pokemon.get(0);

                //Setting the view
                pokemon_name.setText(p.getName());
                pokemon_number.setText(p.getNum());
                pokemon_height.setText(p.getHeight());
                pokemon_weight.setText(p.getWeight());
                Glide.with(context).asBitmap().load(p.getImg()).into(pokemon_image);
                for(int j = 0; j<pokemon_types.size(); j++){
                    try {
                        switch (p.getType().get(j)) {
                            case "Bug":
                                pokemon_types.get(j).setImageResource(R.drawable.bug);
                                break;
                            case "Dark":
                                pokemon_types.get(j).setImageResource(R.drawable.dark);
                                break;
                            case "Dragon":
                                pokemon_types.get(j).setImageResource(R.drawable.dragon);
                                break;
                            case "Electric":
                                pokemon_types.get(j).setImageResource(R.drawable.electric);
                                break;
                            case "Fairy":
                                pokemon_types.get(j).setImageResource(R.drawable.fairy);
                                break;
                            case "Fighting":
                                pokemon_types.get(j).setImageResource(R.drawable.fighting);
                                break;
                            case "Fire":
                                pokemon_types.get(j).setImageResource(R.drawable.fire);
                                break;
                            case "Flying":
                                pokemon_types.get(j).setImageResource(R.drawable.flying);
                                break;
                            case "Ghost":
                                pokemon_types.get(j).setImageResource(R.drawable.ghost);
                                break;
                            case "Grass":
                                pokemon_types.get(j).setImageResource(R.drawable.grass);
                                break;
                            case "Ground":
                                pokemon_types.get(j).setImageResource(R.drawable.ground);
                                break;
                            case "Ice":
                                pokemon_types.get(j).setImageResource(R.drawable.ice);
                                break;
                            case "Normal":
                                pokemon_types.get(j).setImageResource(R.drawable.normal);
                                break;
                            case "Psychic":
                                pokemon_types.get(j).setImageResource(R.drawable.physic);
                                break;
                            case "Poison":
                                pokemon_types.get(j).setImageResource(R.drawable.poison);
                                break;
                            case "Rock":
                                pokemon_types.get(j).setImageResource(R.drawable.rock);
                                break;
                            case "Stell":
                                pokemon_types.get(j).setImageResource(R.drawable.steel);
                                break;
                            case "Water":
                                pokemon_types.get(j).setImageResource(R.drawable.water);
                                break;
                            default:
                                pokemon_types.get(j).setImageResource(R.color.colorAccent);
                        }
                    }catch (IndexOutOfBoundsException e){pokemon_types.get(j).setImageResource(R.color.colorAccent);}
                }
                try{
                    for(int j = 0; j<pokemon_weaknesses.size(); j++){
                        try {
                            switch (p.getWeaknesses().get(j)) {
                                case "Bug":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.bug);
                                    break;
                                case "Dark":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.dark);
                                    break;
                                case "Dragon":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.dragon);
                                    break;
                                case "Electric":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.electric);
                                    break;
                                case "Fairy":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.fairy);
                                    break;
                                case "Fighting":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.fighting);
                                    break;
                                case "Fire":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.fire);
                                    break;
                                case "Flying":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.flying);
                                    break;
                                case "Ghost":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.ghost);
                                    break;
                                case "Grass":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.grass);
                                    break;
                                case "Ground":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.ground);
                                    break;
                                case "Ice":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.ice);
                                    break;
                                case "Normal":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.normal);
                                    break;
                                case "Psychic":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.physic);
                                    break;
                                case "Poison":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.poison);
                                    break;
                                case "Rock":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.rock);
                                    break;
                                case "Stell":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.steel);
                                    break;
                                case "Water":
                                    pokemon_weaknesses.get(j).setImageResource(R.drawable.water);
                                    break;
                                default:
                                    pokemon_weaknesses.get(j).setImageResource(R.color.colorAccent);
                            }
                        }catch (IndexOutOfBoundsException e){pokemon_weaknesses.get(j).setImageResource(R.color.colorAccent);}
                    }
                }catch(NullPointerException e){}
                try{
                    for(int j = 0; j<pokemon_prev.size(); j++){
                        try {
                            Glide.with(context).asBitmap().load("https://www.serebii.net/pokemongo/pokemon/" + p.getPrevEvolution().get(j).getNum() + ".png").into(pokemon_prev.get(j));
                            pokemon_prev.get(j).setVisibility(View.VISIBLE);
                            pokemon_prev.get(j).setClickable(true);
                            final int index = j;
                            pokemon_prev.get(j).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openPokemonActivity(p.getPrevEvolution().get(index).getNum());
                                }
                            });
                        }catch(IndexOutOfBoundsException e){}
                    }
                }catch(NullPointerException e){}
                try{
                    for(int j = 0; j<pokemon_next.size(); j++){
                        try {
                            Glide.with(context).asBitmap().load("https://www.serebii.net/pokemongo/pokemon/" + p.getNextEvolution().get(j).getNum() + ".png").into(pokemon_next.get(j));
                            pokemon_next.get(j).setVisibility(View.VISIBLE);
                            pokemon_next.get(j).setClickable(true);
                            final int index = j;
                            pokemon_next.get(j).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openPokemonActivity(p.getNextEvolution().get(index).getNum());
                                }
                            });
                        }catch(IndexOutOfBoundsException e){}
                    }
                }catch(NullPointerException e){}

            }

            @Override
            public void onFailure(Call<List<Pokemon>> call, Throwable t) {
                error_msg.setVisibility(View.VISIBLE);
                reload.setVisibility(View.VISIBLE);
                reload.setClickable(true);
                holder.setEnabled(false);
                holder.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void openPokemonActivity (String num){
        Toast.makeText(this.context, "opening pokemon num:" + num, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this.context, PokemonDescription.class);
        intent.putExtra("num", num);
        this.context.startActivity(intent);
    }
}
