package com.example.pokdexapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Pokemon {

    @SerializedName("_id")
    @Expose
    private Id id;
    @SerializedName("num")
    @Expose
    private String num;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("type")
    @Expose
    private List<String> type = null;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("candy")
    @Expose
    private String candy;
    @SerializedName("candy_count")
    @Expose
    private CandyCount candyCount;
    @SerializedName("egg")
    @Expose
    private String egg;
    @SerializedName("spawn_chance")
    @Expose
    private SpawnChance spawnChance;
    @SerializedName("avg_spawns")
    @Expose
    private AvgSpawns avgSpawns;
    @SerializedName("spawn_time")
    @Expose
    private String spawnTime;
    @SerializedName("multipliers")
    @Expose
    private Object multipliers;
    @SerializedName("weaknesses")
    @Expose
    private List<String> weaknesses = null;
    @SerializedName("prev_evolution")
    @Expose
    private List<PrevEvolution> prevEvolution = null;
    @SerializedName("next_evolution")
    @Expose
    private List<NextEvolution> nextEvolution = null;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCandy() {
        return candy;
    }

    public void setCandy(String candy) {
        this.candy = candy;
    }

    public CandyCount getCandyCount() {
        return candyCount;
    }

    public void setCandyCount(CandyCount candyCount) {
        this.candyCount = candyCount;
    }

    public String getEgg() {
        return egg;
    }

    public void setEgg(String egg) {
        this.egg = egg;
    }

    public SpawnChance getSpawnChance() {
        return spawnChance;
    }

    public void setSpawnChance(SpawnChance spawnChance) {
        this.spawnChance = spawnChance;
    }

    public AvgSpawns getAvgSpawns() {
        return avgSpawns;
    }

    public void setAvgSpawns(AvgSpawns avgSpawns) {
        this.avgSpawns = avgSpawns;
    }

    public String getSpawnTime() {
        return spawnTime;
    }

    public void setSpawnTime(String spawnTime) {
        this.spawnTime = spawnTime;
    }

    public Object getMultipliers() {
        return multipliers;
    }

    public void setMultipliers(Object multipliers) {
        this.multipliers = multipliers;
    }

    public List<String> getWeaknesses() {
        return weaknesses;
    }

    public void setWeaknesses(List<String> weaknesses) {
        this.weaknesses = weaknesses;
    }

    public List<PrevEvolution> getPrevEvolution() {
        return prevEvolution;
    }

    public void setPrevEvolution(List<PrevEvolution> prevEvolution) {
        this.prevEvolution = prevEvolution;
    }

    public List<NextEvolution> getNextEvolution() {
        return nextEvolution;
    }

    public void setNextEvolution(List<NextEvolution> nextEvolution) {
        this.nextEvolution = nextEvolution;
    }

}

class AvgSpawns {

    @SerializedName("$numberDouble")
    @Expose
    private String $numberDouble;
    @SerializedName("$numberInt")
    @Expose
    private String $numberInt;

    public String get$numberDouble() {
        return $numberDouble;
    }

    public void set$numberDouble(String $numberDouble) {
        this.$numberDouble = $numberDouble;
    }

    public String get$numberInt() {
        return $numberInt;
    }

    public void set$numberInt(String $numberInt) {
        this.$numberInt = $numberInt;
    }

}

class CandyCount {

    @SerializedName("$numberInt")
    @Expose
    private String $numberInt;

    public String get$numberInt() {
        return $numberInt;
    }

    public void set$numberInt(String $numberInt) {
        this.$numberInt = $numberInt;
    }

}

class Id {

    @SerializedName("$oid")
    @Expose
    private String $oid;

    public String get$oid() {
        return $oid;
    }

    public void set$oid(String $oid) {
        this.$oid = $oid;
    }

}

class NextEvolution {

    @SerializedName("num")
    @Expose
    private String num;
    @SerializedName("name")
    @Expose
    private String name;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

class PrevEvolution {

    @SerializedName("num")
    @Expose
    private String num;
    @SerializedName("name")
    @Expose
    private String name;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

class SpawnChance {

    @SerializedName("$numberDouble")
    @Expose
    private String $numberDouble;

    public String get$numberDouble() {
        return $numberDouble;
    }

    public void set$numberDouble(String $numberDouble) {
        this.$numberDouble = $numberDouble;
    }

}